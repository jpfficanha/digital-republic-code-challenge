import { describe, it, expect } from 'vitest';
import calculateCansOfPaint from './calculateCansOfPaint';

describe('calculateCansOfPaint', () => {
  it('should return an object with the quantity of cans needeed to paint a certain area', () => {
    const cans = calculateCansOfPaint(62.16);

    expect(cans).toMatchObject({
      18: 0,
      3.6: 3,
      2.5: 0,
      0.5: 3,
    });
  });
});
