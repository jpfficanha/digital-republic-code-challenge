import { CAN_VARIATIONS, SQUARE_METER_PER_LITER } from '../../../constants';

const calculateCansOfPaint = (totalArea) => {
  let necessaryQuantity = totalArea / SQUARE_METER_PER_LITER;
  const cans = { 18: 0, 3.6: 0, 2.5: 0, 0.5: 0 };

  while (necessaryQuantity >= CAN_VARIATIONS.EXTRA_LARGE) {
    cans[CAN_VARIATIONS.EXTRA_LARGE] += 1;
    necessaryQuantity -= CAN_VARIATIONS.EXTRA_LARGE;
  }

  while (necessaryQuantity >= CAN_VARIATIONS.LARGE) {
    cans[CAN_VARIATIONS.LARGE] += 1;
    necessaryQuantity -= CAN_VARIATIONS.LARGE;
  }

  while (necessaryQuantity >= CAN_VARIATIONS.MEDIUM) {
    cans[CAN_VARIATIONS.MEDIUM] += 1;
    necessaryQuantity -= CAN_VARIATIONS.MEDIUM;
  }

  while (necessaryQuantity >= CAN_VARIATIONS.SMALL) {
    cans[CAN_VARIATIONS.SMALL] += 1;
    necessaryQuantity -= CAN_VARIATIONS.SMALL;
  }

  return cans;
};

export default calculateCansOfPaint;
