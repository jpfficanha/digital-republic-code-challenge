import { describe, it, expect } from 'vitest';
import formatCansIntoString from './formatCansIntoString';

const cans = {
  18: 3,
  3.6: 1,
  2.5: 0,
  0.5: 1,
};

describe('formatCansIntoString', () => {
  it('should return a formated string based on a object', () => {
    expect(formatCansIntoString(cans)).toEqual(
      '3 latas de 18L + 1 lata de 3.6L + 1 lata de 0.5L'
    );
  });

  it('should return a empty string', () => {
    expect(formatCansIntoString({})).toEqual('');
  });
});
