const formatCansIntoString = (cans) => {
  const messages = [];

  Object.keys(cans).forEach((item) => {
    if (cans[item] > 0) {
      const message = `${cans[item]} ${
        cans[item] > 1 ? 'latas' : 'lata'
      } de ${item}L`;

      messages.push(message);
    }
  });

  return messages.length > 0 ? messages.join(' + ') : '';
};

export default formatCansIntoString;
