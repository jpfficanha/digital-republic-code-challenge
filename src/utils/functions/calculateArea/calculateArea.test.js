import { describe, it, expect } from 'vitest';
import calculateArea from './calculateArea';

const wall = {
  width: '7',
  height: '2.5',
  doors: '1',
  windows: '1',
};

describe('calculateArea', () => {
  it('should return the total area of the wall', () => {
    expect(calculateArea(wall)).toBe(13.58);
  });
});
