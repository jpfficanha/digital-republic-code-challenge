import { DOOR_AREA, WINDOW_AREA } from '../../../constants';

const calculateArea = (wall) => {
  const area = parseFloat(wall.height) * parseFloat(wall.width);
  const windowsArea = WINDOW_AREA * Number(wall.windows);
  const doorsArea = DOOR_AREA * Number(wall.doors);

  return area - (windowsArea + doorsArea);
};

export default calculateArea;
