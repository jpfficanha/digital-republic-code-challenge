export { default as calculateArea } from './functions/calculateArea/calculateArea';
export { default as formatCansIntoString } from './functions/formatCansIntoString/formatCansIntoString';
export { default as validateWall } from './validation/validateWall/validateWall';
export { default as calculateCansOfPaint } from './functions/calculateCansOfPaint/calculateCansOfPaint';
