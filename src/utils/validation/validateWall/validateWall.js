import {
  DOOR_AREA,
  MAX_WALL_AREA,
  MIN_WALL_AREA,
  MIN_WALL_HEIGHT,
  WINDOW_AREA,
} from '../../../constants';

const validateWall = (wall, wallNumber) => {
  const area = parseFloat(wall.height) * parseFloat(wall.width);
  const windowArea = WINDOW_AREA * Number(wall.windows);
  const doorArea = DOOR_AREA * Number(wall.doors);
  const errors = [];

  if (area < MIN_WALL_AREA || area > MAX_WALL_AREA) {
    errors.push(
      `A área da parede ${wallNumber} deve ser maior que 1m² e menor que 50m².`
    );
  }

  if (windowArea > area / 2 || doorArea > area / 2) {
    errors.push(
      `A área das janelas e portas devem ser de no máximo 50% da parede ${wallNumber}.`
    );
  }

  if (
    Number(wall.doors) &&
    parseFloat(wall.height) - parseFloat(MIN_WALL_HEIGHT) < 0.3
  ) {
    errors.push(
      `A altura da parede ${wallNumber} deve ser pelos menos 30cm maior que a porta.`
    );
  }

  return errors;
};

export default validateWall;
