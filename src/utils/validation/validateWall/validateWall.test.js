import { describe, it, expect } from 'vitest';

import validateWall from './validateWall';

const validWall = {
  width: '7',
  height: '2.5',
  doors: '1',
  windows: '1,',
};

const notValidWall = {
  width: '7',
  height: '1.9',
  doors: '10',
  windows: '1,',
};

describe('validateWall', () => {
  it('should return no errors', () => {
    expect(validateWall(validWall, 1).length).toEqual(0);
  });

  it('should return errors', () => {
    const wall = validateWall(notValidWall, 1);

    expect(wall.length).toBeGreaterThan(0);
    expect(wall).toContain(
      'A área das janelas e portas devem ser de no máximo 50% da parede 1.'
    );
    expect(wall).toContain(
      'A altura da parede 1 deve ser pelos menos 30cm maior que a porta.'
    );
  });
});
