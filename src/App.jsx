import { useState } from 'react';

import { Form, InfoCard } from './components';

import './global.scss';
import './App.scss';

function App() {
  const [paintCans, setPaintCans] = useState();
  const [totalArea, setTotalArea] = useState(0);

  return (
    <div className="container">
      <div className="section">
        <h1>Calculadora de tinta</h1>
        <div>
          <Form setPaintCans={setPaintCans} setTotalArea={setTotalArea} />
        </div>
        {paintCans ? (
          <>
            <hr className="divider" />
            <div>
              <InfoCard cans={paintCans} totalArea={totalArea} />
            </div>
          </>
        ) : null}
      </div>
    </div>
  );
}

export default App;
