import { render, screen } from './test/utils';
import { describe, it, expect } from 'vitest';

import App from './App';

describe('<App />', () => {
  it('should render the app component', () => {
    render(<App />);
    expect(screen.getByText('Calculadora de tinta')).toBeInTheDocument();
  });
});
