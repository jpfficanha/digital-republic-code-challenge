export const WINDOW_AREA = 1.2 * 2;

export const DOOR_AREA = 0.8 * 1.9;

export const SQUARE_METER_PER_LITER = 5;

export const MIN_WALL_HEIGHT = 1.9;

export const MIN_WALL_AREA = 1;

export const MAX_WALL_AREA = 50;

export const CAN_VARIATIONS = {
  EXTRA_LARGE: 18,
  LARGE: 3.6,
  MEDIUM: 2.5,
  SMALL: 0.5,
};
