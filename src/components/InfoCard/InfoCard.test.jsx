import { render, screen } from '../../test/utils';
import { describe, it, expect } from 'vitest';

import InfoCard from './InfoCard';

const cans = {
  18: 0,
  3.6: 3,
  2.5: 0,
  0.5: 3,
};

const totalArea = 62.16;

describe('<InfoCard />', () => {
  it('should render the info card component with content', () => {
    render(<InfoCard cans={cans} totalArea={totalArea} />);
    expect(screen.getByText('Área aproximada:')).toBeInTheDocument();
    expect(screen.getByText('62.16 metros quadrados')).toBeInTheDocument();
    expect(
      screen.getByText('Quantidade de latas recomendadas:')
    ).toBeInTheDocument();
    expect(
      screen.getByText('3 latas de 3.6L + 3 latas de 0.5L')
    ).toBeInTheDocument();
  });
});
