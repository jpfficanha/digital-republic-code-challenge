import PropTypes from 'prop-types';

import { formatCansIntoString } from '../../utils';

import './styles.scss';

const InfoCard = ({ cans, totalArea }) => {
  return (
    <div className="info-container">
      <span className="title">Área aproximada:</span>
      <span className="description">
        {parseFloat(totalArea).toFixed(2)} metros quadrados
      </span>
      <span className="title">Quantidade de latas recomendadas:</span>
      <span className="description">{formatCansIntoString(cans)}</span>
    </div>
  );
};

InfoCard.propTypes = {
  cans: PropTypes.shape({
    18: PropTypes.number,
    3.6: PropTypes.number,
    2.5: PropTypes.number,
    0.5: PropTypes.number,
  }).isRequired,
  totalArea: PropTypes.number.isRequired,
};

export default InfoCard;
