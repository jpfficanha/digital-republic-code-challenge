import PropTypes from 'prop-types';
import { forwardRef } from 'react';

import TextInput from './TextInput/TextInput';
import NumberInput from './NumberInput/NumberInput';

const InputRef = forwardRef(function Input({ type, ...rest }, ref) {
  if (type === 'number') {
    return <NumberInput {...rest} ref={ref} />;
  }

  return <TextInput {...rest} ref={ref} />;
});

InputRef.propTypes = {
  type: PropTypes.oneOf(['text', 'number']),
};

export default InputRef;
