import PropTypes from 'prop-types';
import { forwardRef } from 'react';

const NumberInput = forwardRef(function Input(props, ref) {
  return (
    <fieldset>
      {props.label && <label htmlFor={props.name}>{props.label}</label>}
      <input type="number" pattern="[0-9]+" ref={ref} min="0" {...props} />
    </fieldset>
  );
});

NumberInput.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  name: PropTypes.string,
};

export default NumberInput;
