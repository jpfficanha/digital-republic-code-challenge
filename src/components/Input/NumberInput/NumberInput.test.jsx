import { describe, it, expect } from 'vitest';
import { render, screen } from '../../../test/utils';

import NumberInput from './NumberInput';

describe('<NumberInput />', () => {
  it('should render the number input with a label', () => {
    render(<NumberInput label="Number input" />);
    expect(screen.getByRole('spinbutton')).toBeInTheDocument();
    expect(screen.getByText('Number input')).toBeInTheDocument();
  });

  it('should render the number input without a label', () => {
    render(<NumberInput />);
    expect(screen.getByRole('spinbutton')).toBeInTheDocument();
    expect(screen.queryByText('Number input')).toBeNull();
  });
});
