import { describe, it, expect } from 'vitest';
import { render, screen } from '../../../test/utils';

import TextInput from './TextInput';

describe('<TextInput />', () => {
  it('should render the text input with a label', () => {
    render(<TextInput label="Text input" />);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
    expect(screen.getByText('Text input')).toBeInTheDocument();
  });

  it('should render the text input without a label', () => {
    render(<TextInput />);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
    expect(screen.queryByText('Text input')).toBeNull();
  });
});
