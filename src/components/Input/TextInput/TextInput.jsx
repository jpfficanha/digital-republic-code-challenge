import PropTypes from 'prop-types';

import { forwardRef } from 'react';

const TextInput = forwardRef(function Input(props, ref) {
  return (
    <fieldset>
      {props.label && <label htmlFor={props.name}>{props.label}</label>}
      <input ref={ref} type="text" autoComplete="off" {...props} />
    </fieldset>
  );
});

TextInput.propTypes = {
  label: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  name: PropTypes.string,
};

export default TextInput;
