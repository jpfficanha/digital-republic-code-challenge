import { describe, it, expect } from 'vitest';
import { render, screen } from '../../test/utils';

import Input from './Input';

describe('<Input />', () => {
  it('should render a input of type text', () => {
    render(<Input type="text" />);
    expect(screen.getByRole('textbox')).toBeInTheDocument();
  });

  it('should render a input of type number', () => {
    render(<Input type="number" />);
    expect(screen.getByRole('spinbutton')).toBeInTheDocument();
  });
});
