import { describe, it, expect } from 'vitest';
import { render, screen } from '../../test/utils';

import Form from './Form';

describe('<Form />', () => {
  it('should render the form component', () => {
    render(<Form />);
    const form = screen.getByRole('form', { name: 'field-form' });
    const button = screen.getByRole('button');
    expect(form).toBeInTheDocument();
    expect(button).toBeInTheDocument();
  });
});
