export const FIELDS = [
  {
    name: 'width',
    type: 'text',
    value: null,
    label: 'Largura (m)',
  },
  {
    name: 'height',
    type: 'text',
    value: null,
    label: 'Altura (m)',
  },
  {
    name: 'doors',
    type: 'number',
    value: 0,
    label: 'Nr de portas',
  },
  {
    name: 'windows',
    type: 'number',
    value: 0,
    label: 'Nr de janelas',
  },
];

export const DEFAULT_VALUES = {
  walls: Array(4).fill(
    FIELDS.reduce(
      (obj, item) => Object.assign(obj, { [item.name]: item.value }),
      {}
    )
  ),
};
