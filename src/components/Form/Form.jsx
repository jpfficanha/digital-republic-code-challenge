import PropTypes from 'prop-types';
import { useState, useEffect } from 'react';
import { useForm, useFieldArray } from 'react-hook-form';

import { Button } from '../';
import FieldList from './FieldList/FieldList';

import { calculateArea, calculateCansOfPaint, validateWall } from '../../utils';

import { DEFAULT_VALUES } from './constants';

import './styles.scss';

const Form = ({ setPaintCans, setTotalArea }) => {
  const [errors, setErrors] = useState([]);

  const { register, control, handleSubmit } = useForm({
    defaultValues: DEFAULT_VALUES,
  });

  const { fields } = useFieldArray({
    control,
    name: 'walls',
  });

  const onSubmit = (data) => {
    setErrors([]);

    const { walls } = data;
    let isValidData = true;

    const mappedWalls = [...walls].map((wall, index) => {
      const area = calculateArea(wall);
      const wallNumber = index + 1;
      const errors = validateWall(wall, wallNumber);

      if (errors.length > 0) {
        isValidData = false;
        setErrors((prev) => [...new Set([...prev, ...errors])]);
      }

      return { ...wall, area };
    });

    if (isValidData) {
      const totalArea = mappedWalls.reduce((prev, curr) => {
        return prev + curr.area;
      }, 0);

      const totalCans = calculateCansOfPaint(totalArea);

      setTotalArea(totalArea);
      setPaintCans(totalCans);
    }
  };

  useEffect(() => {
    if (errors.length > 0) {
      alert(JSON.stringify(errors, '', 4));
    }
  }, [errors]);

  return (
    <div className="form-container">
      <form onSubmit={handleSubmit(onSubmit)} aria-label="field-form">
        <div>
          <FieldList fields={fields} register={register} />
        </div>
        <div className="button-container">
          <Button type="submit">Calcular</Button>
        </div>
      </form>
    </div>
  );
};

Form.propTypes = {
  setPaintCans: PropTypes.func.isRequired,
  setTotalArea: PropTypes.func.isRequired,
};

export default Form;
