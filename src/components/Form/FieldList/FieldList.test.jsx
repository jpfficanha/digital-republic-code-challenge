import { describe, it, expect, vi } from 'vitest';
import { render, screen } from '../../../test/utils';

import FieldList from './FieldList';

const fields = [
  {
    width: null,
    height: null,
    doors: 0,
    windows: 0,
    id: '8e1f40bc-6a44-4ce0-be3a-25fbb0ecf91a',
  },
  {
    width: null,
    height: null,
    doors: 0,
    windows: 0,
    id: '414415b2-7727-4d00-8437-3ab4f4347048',
  },
  {
    width: null,
    height: null,
    doors: 0,
    windows: 0,
    id: '8e47bc83-9f48-479e-9a6b-933070dd8ec9',
  },
  {
    width: null,
    height: null,
    doors: 0,
    windows: 0,
    id: '81791333-e218-4971-8183-99d16601324c',
  },
];

describe('<FieldList />', () => {
  it('should render the fieldlist', () => {
    render(<FieldList fields={fields} register={() => {}} />);
    expect(screen.getAllByRole('textbox').length).toEqual(8);
    expect(screen.getAllByRole('spinbutton').length).toEqual(8);
    expect(screen.getAllByText(/Parede [0-9]/).length).toEqual(4);
  });
});
