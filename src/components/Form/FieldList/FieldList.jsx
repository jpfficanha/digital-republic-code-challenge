import PropTypes from 'prop-types';
import { Children } from 'react';

import { Input } from '../../';

import { FIELDS } from '../constants';

import './styles.scss';

const FieldList = ({ fields, register }) => {
  return (
    <ul className="field-list">
      {fields.map((item, index) => {
        return (
          <li key={item.id}>
            <p className="field-list-title">Parede {index + 1}</p>
            {Children.toArray(
              FIELDS.map((field) => {
                return (
                  <>
                    <Input
                      {...register(`walls.${index}.${field.name}`, {
                        required: true,
                      })}
                      type={field.type}
                      label={field.label}
                    />
                  </>
                );
              })
            )}
          </li>
        );
      })}
    </ul>
  );
};

FieldList.propTypes = {
  fields: PropTypes.array.isRequired,
  register: PropTypes.func.isRequired,
};

export default FieldList;
