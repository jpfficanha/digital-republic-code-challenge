import PropTypes from 'prop-types';

import './styles.scss';

const Button = ({ type = 'button', children, ...rest }) => {
  return (
    <button className="button-default" type={type} {...rest}>
      {children}
    </button>
  );
};

Button.propTypes = {
  type: PropTypes.oneOf(['button', 'submit', 'reset']),
  children: PropTypes.node,
};

export default Button;
