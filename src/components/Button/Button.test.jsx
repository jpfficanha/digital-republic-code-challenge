import { render, screen } from '../../test/utils';
import { describe, it, expect } from 'vitest';

import Button from '../../components/Button/Button';

describe('<Button />', () => {
  it('should render the default button with children', () => {
    render(<Button>Button Component</Button>);

    const button = screen.getByRole('button');

    expect(button).toHaveClass('button-default');
    expect(screen.getByText('Button Component')).toBeInTheDocument();
  });
});
