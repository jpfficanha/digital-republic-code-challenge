# Calculadora de tinta

Aplicação para calcular quantos latas de tinta são necessárias para pintar uma área específica em m².

## 🚀 Instalação

```bash
# Clonar o repositório
$ git clone git@gitlab.com:jpfficanha/digital-republic-code-challenge.git

# Entrar na pasta
$ cd digital-republic-code-challenge

# Instalar as dependências
$ yarn install --frozen-lockfile
```

## 📦 Rodando o projeto

Existe alguns comandos básicos para execução do projeto:

- `yarn dev`: Roda a aplicação em modo de desenvolvimento. Abra [http://localhost:5173](http://localhost:5173) para ver no navegador.
- `yarn test`: Para rodar os testes unitários da aplicação.
